/**
 * @file rotate.cpp
 *
 * Function that performs a swap of three variable (i.e., a rotate).
 *
 * @author Michael J. Decker, Ph.D. <mdecke@bgsu.edu>
 * @date October 2020
 */

void rotate(int & number_one, int & number_two, int & number_three) {
    int temp_one = number_one;
    int temp_two = number_two;
    int temp_three = number_three;

    number_one = temp_three;
    number_two = temp_one;
    number_three = temp_two;
}