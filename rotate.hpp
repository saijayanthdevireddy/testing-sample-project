/**
 * @file rotate.hpp
 *
 * Function that performs a swap of three variable (i.e., a rotate).
 *
 * @author Michael J. Decker, Ph.D. <mdecke@bgsu.edu>
 * @date October 2020
 */

#ifndef INCLUDED_ROTATE_HPP
#define INCLUDED_ROTATE_HPP

void rotate(int & number_one, int & number_two, int & number_three);

#endif
