/**
 * @file test1_rotate.cpp
 *
 * Test rotate function
 *
 * @author Sai Jayanth. Devireddy.
 * @date October 2020
 */
#include <iostream>
#include <cassert>

#include "rotate.hpp"

int main(int argc, char * argv[]) {

    // example test case
    {
        // setup
        int num_one = 3;
        int num_two = 7;
        int num_three = 42;

        int temp_one = num_one;
        int temp_two = num_two;
        int temp_three = num_three;

        // test
        rotate(num_one, num_two, num_three);

        // verify, note, can't test original values as they are overwritten by rotate
        assert(num_one == temp_three);
        assert(num_two == temp_one);
        assert(num_three == temp_two);

    }

    return 0;
}
