/**
 * @file test1_rotate.cpp
 *
 * Test rotate function
 *
 * @author Sai Jayanth. Devireddy.
 * @date October 2020
 */
#include <iostream>
#include <cassert>

#include "rotate.hpp"

int main(int argc, char * argv[]) {

    // example test case
    {
        // setup
        int num_one = 3;
        int num_two = 7;
        int num_three = 42;

        // test
        rotate(num_one, num_two, num_three);
        rotate(num_one, num_two, num_three);
        rotate(num_one, num_two, num_three);

        // verify, note, can't test original values as they are overwritten by rotate
        assert(num_one == 3);
        assert(num_two == 7);
        assert(num_three == 42);

    }

    return 0;
}
